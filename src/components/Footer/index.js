import React from 'react';
import { withRouter } from 'react-router-dom';
import { Container, Row, NavLink } from 'reactstrap';
import './styles.css';

const routeToSourceMap = {
  '/': '/containers/Home/index.js',
  '/home': '/containers/FourOhFour/index.js',
  '/advice': '/containers/Advice/index.js',
  '/cats': '/containers/Cats/index.js',
  '/resources': '/containers/Resources/index.js',
  '/feedback': '/containers/Feedback/index.js',
};

export const Footer = ({ location }) => (
  <footer>
    <Container className='secondary'>
    <Row className='justify-content-center'>
        <NavLink target='_blank' href={`https://gitlab.com/caseyvangroll/career-day/tree/master/src${routeToSourceMap[location.pathname]}`}>
          Source Code
        </NavLink>
    </Row>
    </Container>
  </footer>
);

export default withRouter(Footer);
