import React from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import './styles.css';

export class CustomNavbar extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = { isOpen: false };
  }

  toggle() {
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {
    const { isOpen } = this.state;
    return (
      <Navbar id='navbar' color="light" light expand='sm'>
        <NavbarBrand tag={Link} to="/home">Career Day - Computer Science</NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem><NavLink tag={Link} to="/cats">Cats</NavLink></NavItem>
            <NavItem><NavLink tag={Link} to="/resources">Resources</NavLink></NavItem>
            <NavItem><NavLink tag={Link} to="/feedback">Feedback</NavLink></NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    );
  }
}

export default CustomNavbar;
