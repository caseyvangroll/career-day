import React from 'react';
import { Container, Row, Col, NavLink, Card, CardImg, CardText, CardBody, CardTitle, Badge } from 'reactstrap';
import { amazonAws, codeAcademy, codeSignal, computerphileAi, computerphile, funFunFunction, github, gitlab, hackerRank, khanAcademy, udemy, secretOfCompSci } from './images';
import './styles.css';

export default props => (
  <Container id='resources'>
    <Container>
      <h5>Learn Coding</h5>
      <Row>
        <Col md='6'>
          <NavLink href='https://www.udemy.com/'>
            <Card>
              <Badge color='warning' className='favorite'>Favorite</Badge>
              <CardImg top width="100%" src={udemy} alt="Udemy" />
              <CardBody>
                <CardTitle>Udemy</CardTitle>
                <CardText>
                  Marketplace where experts sell video courses on all sorts of topics.
                  Generally able to find high quality, hands-on content on any topic,
                  and $200+ courses go on sale for $10 every month or two.
                  </CardText>
              </CardBody>
            </Card>
          </NavLink>
        </Col>
        <Col md='6'>
          <NavLink href='https://www.khanacademy.org/computing/computer-science'>
            <Card>
              <CardImg top width="100%" src={khanAcademy} alt="Khan Academy" />
              <CardBody>
                <CardTitle>Khan Academy</CardTitle>
                <CardText>
                  Theory and Big-Picture content on CS. Mostly advanced topics;
                  deals primarily with complexity, algorithms, and cryptography.
                  </CardText>
              </CardBody>
            </Card>
          </NavLink>
        </Col>
      </Row>
      <Row className='second'>
        <Col md='6'>
          <NavLink href='https://www.codeacademy.com/'>
            <Card>
              <CardImg top width="100%" src={codeAcademy} alt="CodeAcademy" />
              <CardBody>
                <CardTitle>CodeAcademy</CardTitle>
                <CardText>
                  Offers limited number of free web lessons with interactive web terminal.
                  Good for on the go or to get low-commitment exposure to a topic.
                    </CardText>
              </CardBody>
            </Card>
          </NavLink>
        </Col>
        <Col md='6'>
          <NavLink href='https://www.youtube.com/watch?v=oHg5SJYRHA0'>
            <Card>
              <CardImg top width="100%" src={secretOfCompSci} alt="Greatest Secret of CS" />
              <CardBody>
                <CardTitle>Greatest Secret of CS</CardTitle>
                <CardText>
                  I wish someone had told me this before I started my CS Journey.
                  This has saved me at least 500 hours since it was explained to me.
                  </CardText>
              </CardBody>
            </Card>
          </NavLink>
        </Col>
      </Row>
    </Container>

    <Container>
      <h5>Youtube</h5>
      <Row>
        <Col md='6'>
          <NavLink href='https://www.youtube.com/channel/UCO1cgjhGzsSYb1rsB4bFe4Q'>
            <Card>
              <CardImg top width="100%" src={funFunFunction} alt="Fun Fun Function" />
              <CardBody>
                <CardTitle>Fun Fun Function</CardTitle>
                <CardText>
                  Energetic and topical short videos, mostly on Javascript.
                  MPJ is a popular voice in the webdev community.
                  </CardText>
              </CardBody>
            </Card>
          </NavLink>
        </Col>
        <Col md='6'>
          <NavLink href='https://www.youtube.com/watch?v=tlS5Y2vm02c&list=PLzH6n4zXuckquVnQ0KlMDxyT5YE-sA8Ps'>
            <Card>
              <Badge color='warning' className='favorite'>Favorite</Badge>
              <CardImg top width="100%" src={computerphileAi} alt="Artificial Intelligence" />
              <CardBody>
                <CardTitle>Artificial Intelligence</CardTitle>
                <CardText>
                  This is an absolutely delightful series of mind experiments
                  around AI and AI Safety. Zero prerequisites, mostly logic.
                  </CardText>
              </CardBody>
            </Card>
          </NavLink>
        </Col>
      </Row>
      <Row className='second'>
        <Col md='6'>
          <NavLink href='https://www.youtube.com/channel/UC9-y-6csu5WGm29I7JiwpnA'>
            <Card>
              <CardImg top width="100%" src={computerphile} alt="Computerphile" />
              <CardBody>
                <CardTitle>Computerphile</CardTitle>
                <CardText>
                  Youtube channel with quality videos on most any CS topic you
                  can think of. These people have other jobs, but somehow find
                  time to explain complex topics well enough for non-specialists.
                  </CardText>
              </CardBody>
            </Card>
          </NavLink>
        </Col>
      </Row>
    </Container>

    <Container>
      <h5>Practice Coding</h5>
      <Row>
        <Col md='6'>
          <NavLink href='https://codesignal.com/'>
            <Card>
              <Badge color='warning' className='favorite'>Favorite</Badge>
              <CardImg top width="100%" src={codeSignal} alt="CodeSignal" />
              <CardBody>
                <CardTitle>CodeSignal</CardTitle>
                <CardText>
                  My favorite way to keep up coding chops. They've game-ified coding
                  in a satisfying way, and their challenges have reliably thorough and
                  sane test cases to help you along with your algorithms.
                  </CardText>
              </CardBody>
            </Card>
          </NavLink>
        </Col>
        <Col md='6'>
          <NavLink href='https://www.hackerrank.com/'>
            <Card>
              <CardImg top width="100%" src={hackerRank} alt="HackerRank" />
              <CardBody>
                <CardTitle>HackerRank</CardTitle>
                <CardText>
                  An alternative to CodeSignal, though it is less intuitive and the
                  test cases aren't as smoothly ramped up to be useful as CodeSignal.
                  </CardText>
              </CardBody>
            </Card>
          </NavLink>
        </Col>
      </Row>
    </Container>

    <Container>
      <h5>Hosting</h5>
      <Row>
        <Col md='6'>
          <NavLink href='https://about.gitlab.com/'>
            <Card>
              <Badge color='warning' className='favorite'>Favorite</Badge>
              <CardImg top width="100%" src={gitlab} alt="GitLab" />
              <CardBody>
                <CardTitle>GitLab</CardTitle>
                <CardText>
                  My favorite one-stop-shop for personal coding projects. We use an enterprise
                  version of this at my workplace, but for free they will host as much code as you'll
                  ever need with all sorts of useful integrations like CICD, webpages, and documentation.
                  </CardText>
              </CardBody>
            </Card>
          </NavLink>
        </Col>
        <Col md='6'>
          <NavLink href='https://github.com/'>
            <Card>
              <CardImg top width="100%" src={github} alt="GitHub" />
              <CardBody>
                <CardTitle>GitHub</CardTitle>
                <CardText>
                  Pretty similiar to GitLab, though contains fewer add-ons. Reliable and free as well.
                  </CardText>
              </CardBody>
            </Card>
          </NavLink>
        </Col>
      </Row>
      <Row className='second'>
        <Col md='6'>
          <NavLink href='https://aws.amazon.com/'>
            <Card>
              <CardImg top width="100%" src={amazonAws} alt="AWS" />
              <CardBody>
                <CardTitle>Amazon Web Services</CardTitle>
                <CardText>
                  Undisputedly the top dog in Cloud Services, AWS is a massive complicated intimidating beast,
                  but if you take the time to learn one or two of its facets like S3 Storage or EC2 instances,
                  perhaps with Udemy - they have plenty of courses - it will be a huge boost to any resume.
                  They offer your first year of services for free - hard to pass up!
                  </CardText>
              </CardBody>
            </Card>
          </NavLink>
        </Col>
      </Row>
    </Container>
  </Container>
);