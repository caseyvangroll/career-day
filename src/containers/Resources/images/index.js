export { default as amazonAws} from './amazon-aws.png';
export { default as codeAcademy } from './code-academy.png';
export { default as codeSignal } from './code-signal.png';
export { default as computerphileAi } from './computerphile-ai.jpg';
export { default as computerphile } from './computerphile.jpg';
export { default as funFunFunction } from './fun-fun-function.jpg';
export { default as github } from './github.jpg';
export { default as gitlab } from './gitlab.png';
export { default as hackerRank } from './hacker-rank.png';
export { default as khanAcademy } from './khan-academy.jpg';
export { default as udemy } from './udemy.png';
export { default as secretOfCompSci } from './secret-of-computer-science.gif';

