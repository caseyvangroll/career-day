/* eslint-disable no-useless-escape */
import React from 'react';
import { Container, Alert, Form, FormGroup, Label, Input, Button, Card, CardBody, Spinner } from 'reactstrap';
import axios from 'axios';
import camelCase from 'camel-case';
import './styles.css';

const possibleInterests = [
  'Software Development',
  'Software Testing',
  'Hardware',
  'Web and Networking',
  'Security',
  'DevOps',
  'System Administration',
];

const makeID = text => `interestedIn${camelCase(text)}`;
const validateEmail = email => /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(String(email).toLowerCase());

export class Feedback extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      interestLevel: '',
      ...possibleInterests.map(makeID).reduce((acc, e) => ({ ...acc, [e]: false }), {}),
      otherChecked: false,
      otherInterest: '',
      notes: ''
    };
    this.fieldChange = this.fieldChange.bind(this);
    this.submit = this.submit.bind(this);
  }

  fieldChange(event) {
    let { name, value } = event.target;
    if (/(interestedIn|otherChecked)/.test(name)) {
      this.setState({
        ...this.state,
        [name]: !this.state[name]
      });
    } else {
      this.setState({ ...this.state, [name]: value });
    }
  }

  submit() {
    this.setState({ submission: 'pending' });
    axios.post('https://career-day-mailer.herokuapp.com/', this.state)
      .then(() => {
        this.setState({ submission: 'success' })
      }).catch(error => {
        this.setState({ submission: 'error' })
      })
  }

  isReady() {
    const { name, email, interestLevel } = this.state;
    return name !== '' && interestLevel !== '' && validateEmail(email);
  }

  render() {
    const { name, email, interestLevel, interestsChecked, otherChecked, otherInterest, notes, ...isChecked } = this.state;

    const interestScaleRadioButtons = [
      'very', 'pretty', 'kind of', 'not at all'
    ].map(text => (
      <FormGroup key={text} check>
        <Label check for={text}>
          <Input
            type="radio"
            name='interestLevel'
            value={text}
            id={text}
            checked={interestLevel === text}
            onChange={this.fieldChange}
            required
          />
          {text}
        </Label>
      </FormGroup>
    ));

    const interestCheckboxes = possibleInterests
      .map(text => [text, makeID(text)])
      .map(([text, id]) => (
        <FormGroup key={id} check>
          <Label check for={id}>
            <Input
              type="checkbox"
              name={id}
              id={id}
              checked={isChecked[id]}
              onChange={this.fieldChange}
              required
            />
            {text}
          </Label>
        </FormGroup>
      ));

    const otherInterestCheckbox = (
      <FormGroup check>
        <Label check for='other'>
          <Input type="checkbox" name='otherChecked' id="other" checked={otherChecked} onChange={this.fieldChange} />
          Other
        </Label>
        {
          otherChecked
          ? <Input type='text' name='otherInterest' value={otherInterest} id='otherInterest' onChange={this.fieldChange}/>
          : null
        }
      </FormGroup>
      );

    let submitSection = <Button disabled={!this.isReady()} onClick={this.submit}>Submit</Button>;
    if (this.state.submission === 'error') {
      submitSection = <Alert color='danger'>Error submitting. Please try again later.</Alert>;
    } else if (this.state.submission === 'pending') {
      submitSection = <Spinner color='info'></Spinner>;
    } else if (this.state.submission === 'success') {
      submitSection = <Alert color='success'>Thanks! I'll get in touch with you soon.</Alert>;
    } 

    return (
      <Container id='feedback'>
        <Alert color='info'>
          This form will submit to my email if you'd like to learn more about Computer Science,
          its various education/career paths, or network with people at UWM/Computer Society or Northwestern Mutual!
        </Alert>

        <Form>
          <FormGroup>
            <Label for="name">Name</Label>
            <Input type="text" name="name" id="name" value={name} onChange={this.fieldChange} />
          </FormGroup>

          <FormGroup>
            <Label for="email">Email</Label>
            <Input type="email" name="email" id="email" value={email} onChange={this.fieldChange} />
          </FormGroup>

          <Card>
            <CardBody>
              <Label>I am _____ interested in Computer Science.</Label>
              <FormGroup tag='fieldset'>
                {interestScaleRadioButtons}
              </FormGroup>
            </CardBody>
          </Card>

          <Card>
            <CardBody>
              <Label>I am interested in...</Label>
              {interestCheckboxes}
              {otherInterestCheckbox}
            </CardBody>
          </Card>

          <FormGroup>
            <Label for="notes">Feedback for Presentation</Label>
            <Input type="textarea" name="notes" id="notes" value={notes} onChange={this.fieldChange} />
          </FormGroup>

          {submitSection}
        </Form>
      </Container>
    );
  }
};

export default Feedback;