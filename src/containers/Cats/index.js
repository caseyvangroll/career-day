import React from 'react';
import { Carousel, CarouselItem, CarouselControl, CarouselIndicators, CarouselCaption, Container } from 'reactstrap';
import './styles.css';
import getCatImage from './images';

const fitz = [
  'Up close and personal with the Fitz',
  'The Lord of the Flowers',
  'The Watcher on the Wall',
  'Full nugget mode: engage!',
  'This is the best cat in the world',
  'A snugged little bug in a rug',
  'I am your master.',
];

const poe = [
  'A True Gremlin',
  'Goodnight Sweet Prince',
  'Derp City - Population 1',
  'A Delicate Flower',
  'WHO? ME?',
  'Being a cat is really hard.',
  'Look at my belly! (But don’t touch it!)',
];

const items = [
  ...fitz.map((caption, i) => ({ src: getCatImage('fitz', i + 1), altText: `Fitz ${i + 1}`, caption })),
  ...poe.map((caption, i) => ({ src: getCatImage('poe', i + 1), altText: `Poe ${i + 1}`, caption })),
];

export class Cats extends React.Component {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    this.setState({ activeIndex: newIndex });
  }

  render() {
    const { activeIndex } = this.state;

    const slides = items.map((item) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.src}
        >
          <img src={item.src} alt={item.altText} />
          <CarouselCaption captionText={item.caption} />
        </CarouselItem>
      );
    });

    return (
      <Container>
        <Carousel
          activeIndex={activeIndex}
          next={this.next}
          previous={this.previous}
        >
          <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
          {slides}
          <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous}/>
          <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
        </Carousel>
      </Container>
    );
  }
}

export default Cats;
