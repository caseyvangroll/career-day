export { default as Feedback } from './Feedback';
export { default as Cats } from './Cats';
export { default as Resources } from './Resources';
export { default as Home } from './Home';
export { default as FourOhFour } from './FourOhFour';
