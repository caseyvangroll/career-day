import React from 'react';
import { Container, Jumbotron, UncontrolledCollapse, Button, Card, CardBody, NavLink } from 'reactstrap';
import './styles.css';

export default props => (
  <Container>
    <Jumbotron color='primary' id='home-jumbo'>
      <h1 className='display-3'>Welcome to Career Day!</h1>
      <hr className="my-2" />
      <p style={{ fontSize: '1.3rem', marginTop: '3rem', marginBottom: '3rem' }}>Our goal today is simple: find as many bugs as you can.</p>

      <Button color='primary' id='toggler'>Our Definition of Bug</Button>
      <UncontrolledCollapse toggler='#toggler'>
        <Card>
          <CardBody>
            <h6>A flaw in the website that either...</h6>
            <p style={{ textAlign: 'center' }}>
              Causes it to behave inconsistent with expectations
                <br />
              <em>or</em>
              <br />
              Prevents/hinders the user from performing some task
                </p>
          </CardBody>
        </Card>
      </UncontrolledCollapse>

      <Button color='primary' id='toggler2'>What to note for each Bug</Button>
      <UncontrolledCollapse  toggler='#toggler2'>
        <Card>
          <CardBody>
            <h6>1. Where to find and/or how to recreate the behavior - be as specific as possible!</h6>
            <h6 style={{ fontWeight: 'normal', paddingLeft: '2rem' }}>Try to isolate bugs from eachother. To reset the behavior try reloading
                <NavLink style={{ display: 'inline', padding: '0.2rem' }} href='https://caseyvangroll.gitlab.io/career-day'>the homepage</NavLink>.
                </h6>
            <h6>2. Why you think it's a bug.</h6>
            <h6>3. Any other notes you think might be useful.</h6>
          </CardBody>
        </Card>
      </UncontrolledCollapse>

      <Button color='primary' id='toggler3'>Some things to try...</Button>
      <UncontrolledCollapse toggler='#toggler3'>
        <Card>
          <CardBody>
            <h6>1. Clicking buttons, clicking buttons again</h6>
            <h6>2. Reading carefully</h6>
            <h6>3. Resizing the browser window</h6>
            <h6>4. Checking all links go to correct places</h6>
          </CardBody>
        </Card>
      </UncontrolledCollapse>
    </Jumbotron>
  </Container>
);