import React from 'react';
import { Container, Alert } from 'reactstrap';

export default props => (
  <Container>
    <Alert color='danger'>Uh oh! Page not found.</Alert>
  </Container>
);
