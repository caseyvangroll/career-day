import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Switch, Route } from 'react-router-dom';
import { Navbar, Footer } from './components';
import { Home, Feedback, Cats, Resources, FourOhFour } from './containers';
import 'bootstrap/dist/css/bootstrap.css';
import './styles.css';

const App = () => (
  <HashRouter>
    <div>
      <Navbar />
      <main>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/cats" component={Cats} />
          <Route exact path="/resources" component={Resources} />
          <Route exact path="/feedback" component={Feedback} />
          <Route path='/' component={FourOhFour} />
        </Switch>
      </main>
      <Footer />
    </div>
  </HashRouter>
);

ReactDOM.render(<App />, document.getElementById('root'));
